; Drush Make stub for the OpenBeerfest distribution
;
; Use this file to build a full distribution including Drupal core and the
; OpenBeerfest install profile using the following command:
;
; drush make build-openbeerfest.make <target directory>

api = 2
core = 7.x

; Drupal core
includes[] = drupal-org-core.make

projects[openbeerfest][type] = profile
projects[openbeerfest][download][type] = git
projects[openbeerfest][download][url] = http://git.drupal.org/project/openbeerfest.git
projects[openbeerfest][download][branch] = 7.x-1.x

